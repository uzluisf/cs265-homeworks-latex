### Latex source for CS265 HWs

> **Note**: The possibility of a  successful compilation increases if you've 
> an almost complete and recent Tex Live distribution installed in your machine.

Run `make` to compile the tex files in the `homeworks` directory. The PDFs
alongside the other files created during the compilation will be placed
in the root directory. To remove everything except the PDFs, run `make clean`.
To remove everything, run `make clean-all`.
