hw_dir=homeworks
sources=$(wildcard $(hw_dir)/*.tex)

pdfs=$(sources:.tex=.pdf)

all: $(pdfs)

%.pdf : %.tex
	pdflatex $<

clean:
	rm -f ./*.out ./*.aux ./*.synctex.gz ./*.log ./*.out

clean-all:
	rm -f ./*.out ./*.aux ./*.synctex.gz ./*.log ./*.out ./*.pdf
